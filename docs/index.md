---
layout: home

hero:
  name: 蒸汽机里の业余小码农
  text: Just Coding... 
  tagline: Talk is cheap, show me your code!
  image:
    src: /img/logo.png
    alt: VitePress
  actions:
    - theme: brand
      text:  Welcome!
      link: /colorfulLife/
    - theme: alt
      text: GitLab
      link: https://gitlab.com/lijing-2008/coderli-vitepress
features:
  - icon: 🧑🏼‍💻‍
    title: 一个业余程序爱好者
    details: 人的一生很短暂，多做一些自己感兴趣的事...
  - icon: 🥷🏻
    title: 学过的语言
    details: C,Java,Python,Rust,JS,TS,Scala,Swift...
  - icon: 🔋
    title: 喜欢的框架
    details: SpringBoot,NestJS,Vue,React...
---

